﻿using System;
using System.IO;
using System.Text.RegularExpressions;

namespace FindAirport
{
    public class Airport
    {
        private string _code;
        private string _city;
        private double _lat;
        private double _lng;
        private int _elevM;
        public delegate void LoggerDelegate(string msg);
        public static LoggerDelegate LogFailSet;

        const string DatafileName = @"..\..\airports.txt";

        public string Code
        {
            get { return _code; }
            set
            {
                // Create a pattern for code that consist with 3 uppercase letters
                string codePattern = @"^[A-Z]{3}$";
                // Create a Regex  
                Regex rgCode = new Regex(codePattern);
                if (!rgCode.IsMatch(value))
                {
                    throw new InvalidDataException(ErrorMessages.CODE_AIRPORT_ERROR_VALIDATION);
                }
                _code = value;
            }
        }

        public string City
        {
            get { return _city; }
            set
            {
                // Create a pattern for city that are 1-50 characters, made up of uppercase and lowercase letters, digits, and .,- characters
                string cityPattern = @"[a-zA-Z0-9,\.\-]{1,50}$";
                // Create a Regex  
                Regex rgCity = new Regex(cityPattern);
                if (!rgCity.IsMatch(value))
                {
                    throw new InvalidDataException("City needs to be validated");
                }
                _city = value;
            }
        }

        public double Lat
        {
            get { return _lat; }
            set
            {
                if (value < -90 || value > 90)
                {
                    throw new InvalidDataException("Latitude must be between -90 and 90 degrees");
                }
                _lat = value;
            }
        }

        public double Lng
        {
            get { return _lng; }
            set
            {
                if (value < -180 || value > 180)
                {
                    throw new InvalidDataException("Longitude must be between -180 and 180 degrees");
                }
                _lng = value;
            }
        }

        public int ElevM
        {
            get { return _elevM; }
            set
            {
                if (value < -1000 || value > 10000)
                {
                    throw new InvalidDataException("Elevation must be between -1000 and 10000 meters");
                }
                _elevM = value;
            }
        }

        public Airport(string code, string city, double lat, double lng, int elevM)
        {
            Code = code;
            City = city;
            Lat = lat;
            Lng = lng;
            ElevM = elevM;
            //Logging($"Airport {code} was created.");
        }

        public Airport(string dataLine)
        {
            string[] dataInput = dataLine.Split(';');
            if (dataInput.Length != 5)
            {
                //invoke the logger
                throw new InvalidDataException("Line has a problem.");
            }
            Code = dataInput[0];
            City = dataInput[1];
            double lat;
            if (!double.TryParse(dataInput[2], out lat))
            {
                Console.WriteLine("Latitude is not correct");
                return;
            }
            else
            {
                Lat = lat;
            }
            double lng;
            if (!double.TryParse(dataInput[3], out lng))
            {
                Console.WriteLine("Longitude is not correct");
                return;
            }
            else
            {
                Lng = lng;
            }
            int elevM;
            if (!int.TryParse(dataInput[4], out elevM))
            {
                Console.WriteLine("elevM is not correct");
                return;
            }
            else
            {
                ElevM = elevM;
            }
        }

        public override string ToString()
        {
            return $"{Code} in {City} at {Lat} lat / {Lng} lng at {ElevM}m elevation";
        }

        public string ToDataString()
        {
            return $"{Code};{City};{Lat};{Lng};{ElevM}";
        }
    }
}
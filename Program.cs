﻿using System;
using System.Collections.Generic;
using System.Device.Location;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FindAirport
{
    public class Program
    {
        const string DatafileName = @"..\..\data.txt";
        const string LogfileName = @"..\..\events.log";
        static List<Airport> AirportList = new List<Airport>();
        static void Main(string[] args)
        {
            if (LoadFromFile())
            {
                while (true)
                {
                    int choice = GetMenuChoices();
                    switch (choice)
                    {
                        case 1:
                            //you need to ask user to enter the information
                            Console.WriteLine("Please enter the code");
                            string code = Console.ReadLine();
                            Console.WriteLine("Please enter the city");
                            string city = Console.ReadLine();
                            Console.WriteLine("Please enter the latitude");
                            string latitudeStr = Console.ReadLine();
                            double latitude;
                            if (!double.TryParse(latitudeStr, out latitude))
                            {
                                Console.WriteLine("Please enter a number for latitude");
                                return;
                            }
                            Console.WriteLine("Please enter the latitude");
                            string longitudeStr = Console.ReadLine();
                            double longitude;
                            if (!double.TryParse(longitudeStr, out longitude))
                            {
                                Console.WriteLine("Please enter a number for longitude");
                                return;
                            }
                            Console.WriteLine("Please enter the elevation");
                            string elevationStr = Console.ReadLine();
                            int elevation;
                            if (!int.TryParse(longitudeStr, out elevation))
                            {
                                Console.WriteLine("Please enter a number for elevation");
                                return;
                            }

                            Airport airport = new Airport(code, city, latitude, longitude, elevation);
                            AirportList.Add(airport);
                            break;
                        case 2:
                            ListAllAirports();
                            break;
                        case 3:
                            //ask user to give the name
                            Console.WriteLine("Please eneter latitude");
                            string latStr = Console.ReadLine();
                            double lat;
                            if (!double.TryParse(latStr, out lat))
                            {
                                Console.WriteLine("Please enter a number for latitude");
                                return;
                            }
                            Console.WriteLine("Please enter the latitude");
                            string lonStr = Console.ReadLine();
                            double lon;
                            if (!double.TryParse(lonStr, out lon))
                            {
                                Console.WriteLine("Please enter a number for longitude");
                                return;
                            }
                            string searchName = Console.ReadLine();
                            Console.WriteLine(FindNearestAirport(lat, lon));
                            break;
                        case 4:
                            FindAirportElevation();
                            break;
                        case 5:
                            //ask user to enter the choice
                            Console.WriteLine(
                                @"Changing logging settings:
1-Logging to console
2-Logging to file
Enter your choices, comma-separated, empty for none: 1,2");

                            string logChoice = Console.ReadLine();
                            Airport.LogFailSet = null;
                            if (logChoice == "1")
                            {
                                Airport.LogFailSet += LogToConsole;
                            }
                            else if (logChoice == "2")
                            {
                                Airport.LogFailSet += LogToFile;
                            }
                            else if (logChoice == "1,2")
                            {
                                Airport.LogFailSet += LogToFile;
                                Airport.LogFailSet += LogToConsole;
                            }
                            else
                            {
                                Airport.LogFailSet = null;
                                Console.WriteLine("LABASLBLASBVL");
                            }


                            //logging option
                            break;
                        case 0:
                            SaveToFile(AirportList);
                            return;
                        default:
                            Console.WriteLine("invalid Number");
                            break;
                    }
                }

            }

            Console.ReadLine();
        }

        private static void AddAirportInfo(Airport airport)
        {
            Console.WriteLine("Adding airport");
            AirportList.Add(airport);
            Console.WriteLine("Airport added");
        }


        private static void ListAllAirports()
        {
            foreach (Airport a in AirportList)
            {
                Console.WriteLine(a);
            }
        }

        public static double FindNearestAirport(double latitude, double longitude)
        {
            Console.WriteLine(("enter the code of the airport"));
            string airportCode = Console.ReadLine();
            Airport foundAirport = AirportList.Find(airp => airp.Code.Equals(airportCode));
            GeoCoordinate coordinate = new GeoCoordinate(foundAirport.Lat, foundAirport.Lng);

            List<>
            var nearest = locations.Select(x => new GeoCoordinate(x.Latitude, x.Longitude))
                .OrderBy(x => x.GetDistanceTo(coordinate))
                .First();
            return nearest;
        }

        private static void FindAirportElevation()
        {
            //TODO:
            double elevation = 0;
            foreach (Airport a in AirportList)
            {
                elevation += a.ElevM;
            }
            Console.WriteLine($"For all airports the standard deviation of their elevation is {elevation}");

        }

        private static void ChangeLogDelegates(string log)
        {

        }
        private static bool LoadFromFile()
        {
            if (File.Exists(DatafileName))
            {
                try
                {
                    string[] lineArray = File.ReadAllLines(DatafileName);
                    foreach (string line in lineArray)
                    {
                        Airport airport = new Airport(line);
                        AirportList.Add(airport);
                    }

                    return true;
                }
                catch (IOException exception)
                {
                    Airport.LogFailSet?.Invoke("error in the file " + exception.Message);
                    Console.WriteLine("error in the file " + exception.Message);
                    return false;
                }
            }

            return false;

        }

        public static void SaveDataToFile()
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(DatafileName))
                {
                    foreach (Airport airport in AirportList)
                    {
                        writer.WriteLine(airport.ToDataString());
                    }
                }
            }
            catch (IOException exc)
            {
                //TODO:
            }
        }

        private static void SaveToFile(List<Airport> listAirports)
        {
            using (StreamWriter outputFile = new StreamWriter(DatafileName))
            {
                foreach (Airport airport in listAirports)
                {
                    //outputFile.WriteLine(person.Name+";"+person.Age+";"+person.City);
                    //interpolated string 
                    outputFile.WriteLine($"{airport.Code};{airport.City};{airport.Lat};{airport.Lng};{airport.ElevM}");
                }
            }


        }
        public static void LogToFile(string log)
        {
            using (StreamWriter outputFile = new StreamWriter(LogfileName))
            {
                log = DateTime.Now + log;
                outputFile.WriteLine(log);
            }
            Console.WriteLine("Logging to file enabled.");
        }
        //another method to show in the console.
        public static void LogToConsole(string log)
        {
            Console.WriteLine("Logging to console enabled");
        }

        public static void AddAirport(Airport airport)
        {
            AirportList.Add(airport);
        }


        private static int GetMenuChoices()
        {
            while (true)
            {
                Console.Write(
@"1-Add Airport
2-List all airports
3-Find nearest airport by code
4-Find airport's elevation standard deviation
5-Change log delegates
0-Exit          
");
                string choiceStr = Console.ReadLine();
                int choice;
                if (!int.TryParse(choiceStr, out choice))
                {
                    Console.WriteLine("Value must be a number between 0 to 5 ");
                    continue;
                }
                return choice;
            }
        }
    }
}

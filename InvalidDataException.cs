﻿using System;

namespace FindAirport
{
    public class InvalidDataException : Exception
    {
        public InvalidDataException(string msg) : base(msg)
        {

        }
    }
}